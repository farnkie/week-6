import bos.MoveRandomly;
import bos.NoMove;
import bos.RelativeMove;

import java.awt.*;
import java.util.Optional;

public class Shepherd extends Character {

    Behaviour behaviour;

    public Shepherd(Cell location, Behaviour behaviour) {
        super(location, behaviour);
        display = Optional.of(Color.GREEN);
    }
}