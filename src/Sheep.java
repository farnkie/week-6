import bos.MoveRandomly;
import bos.NoMove;
import bos.RelativeMove;

import java.awt.*;
import java.util.Optional;
import java.util.List;
public class Sheep extends Character {

    Behaviour behaviour;

    public Sheep(Cell location, Behaviour behaviour) {
        super(location, behaviour);
        display = Optional.of(Color.WHITE);
    }
}